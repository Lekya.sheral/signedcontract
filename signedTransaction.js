var Tx = require('ethereumjs-tx').Transaction
const Web3 = require('web3')
const web3 = new Web3("https://ropsten.infura.io/v3/cdecd4aa6ade483e93b14b0cdeef6111")



const account1 = '0x176895200e48ceABb42F33E76662aC6233A51783'
const account2 = '0x771A41D64F9d56067ac24d7d88d7F4F5a33eF122'
const privateKey1 = Buffer.from('E7FEAAB07AC93ABED5E589165C5CE512D0C4436F9C734F89359B0C63A67259F4', 'hex')
//const chainId = '3'

web3.eth.getBalance(account1, (err, bal) => {
    console.log("balance:", web3.utils.fromWei(bal, 'ether'))
}) 

web3.eth.getTransactionCount(account1, (err, txCount) => {
    // Build the transaction
    const txObject = {
      from:     account1,
      nonce:    web3.utils.toHex(txCount),
      to:       account2,
      value:    web3.utils.toHex(web3.utils.toWei('0.1', 'ether')),
      gasLimit: web3.utils.toHex(21000),
      gasPrice: web3.utils.toHex(web3.utils.toWei('10', 'gwei'))
      
    }

    

    console.log(txObject)

    const tx = new Tx(txObject)
    //const chainId = tx.getChainId()
    //tx.v = Buffer.from(chainId)
    //tx.nonce = await web3.utils.toHex(web3.eth.getTransactionCount(account1))
    tx.sign(privateKey1)
  
    const serializedTx = tx.serialize()
    const raw = '0x' + serializedTx.toString('hex')

    web3.eth.sendSignedTransaction(raw, (err, txHash) => {
        console.log('err:', err)
        console.log('txHash:', txHash)
        // Now go check etherscan to see the transaction!
  })

})